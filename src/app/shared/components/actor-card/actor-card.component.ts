import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Actor } from '../../models/actor.model';

@Component({
  selector: 'app-actor-card',
  templateUrl: './actor-card.component.html',
  styleUrls: ['./actor-card.component.scss'],
})
export class ActorCardComponent implements OnInit {
  @Input() actor: Actor;
  @Output() movieClicked = new EventEmitter<void>();

  constructor(private router: Router) { }

  ngOnInit() {}

  onMovieClick(id) {
    this.router.navigate(['/movies', id]);
  }

}
