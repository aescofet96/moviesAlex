import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bornCityLength'
})
export class BornCityLengthPipe implements PipeTransform {

  transform(bornCity: string): string {
    bornCity = bornCity.split('-').join(' ');
      if(bornCity.length > 13){
        bornCity = bornCity.split('').splice(0,13).join('')+'...';
      }
    return bornCity;
  }

}
