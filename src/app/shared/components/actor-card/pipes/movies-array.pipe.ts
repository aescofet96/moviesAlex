import { Pipe, PipeTransform } from '@angular/core';
import { MOVIES } from 'src/app/shared/backend';

@Pipe({
  name: 'moviesArray',
})
export class MoviesArrayPipe implements PipeTransform {
  transform(movie: number): string {
        const movieConverted = MOVIES.find((m) => m.id === movie);
        return movieConverted?.title;
  }
}
