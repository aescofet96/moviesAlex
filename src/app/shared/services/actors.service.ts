import { Injectable } from '@angular/core';
import { ACTORS, MOVIES } from '../backend';
import { Actor } from '../models/actor.model';

@Injectable({
  providedIn: 'root',
})
export class ActorsService {
  actors: Actor[] = [...ACTORS];

  constructor() {}

  getActors(): Actor[] {
    return this.actors;
  }

  getActor(id: number): Actor {
    return this.actors.find((actor) => actor.id === id);
  }
}
